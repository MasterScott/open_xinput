/* Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the xinput open-source project.
 *
 * xinput open-source project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * xinput open-source project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with xinput open-source project.  If not, see <https://www.gnu.org/licenses/>
 */

#include <Windows.h>
#include <Xinput.h>

#include <iostream>

#pragma comment(lib, "Xinput1_3.lib")

#define XINPUT_GAMEPAD_GUIDE 0x0400

extern "C" DWORD WINAPI XInputGetMaxController();

void print_state(XINPUT_STATE const& state)
{
    std::cout << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)        << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)      << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)      << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)     << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_START)          << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK)           << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB)     << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB)    << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)  << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_GUIDE)          << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_A)              << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_B)              << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_X)              << ' '
              << (bool)(state.Gamepad.wButtons & XINPUT_GAMEPAD_Y)              << ' ';
    std::cout << std::endl;
}

void print_keystroke(XINPUT_KEYSTROKE const& ks)
{
    switch (ks.VirtualKey)
    {
        case VK_PAD_A               : std::cout << "VK_PAD_A" << ' ' << ks.Flags; break;
        case VK_PAD_B               : std::cout << "VK_PAD_B" << ' ' << ks.Flags; break;
        case VK_PAD_X               : std::cout << "VK_PAD_X" << ' ' << ks.Flags; break;
        case VK_PAD_Y               : std::cout << "VK_PAD_Y" << ' ' << ks.Flags; break;
        case VK_PAD_RSHOULDER       : std::cout << "VK_PAD_R1" << ' ' << ks.Flags; break;
        case VK_PAD_LSHOULDER       : std::cout << "VK_PAD_L1" << ' ' << ks.Flags; break;
        case VK_PAD_LTRIGGER        : std::cout << "VK_PAD_L2" << ' ' << ks.Flags; break;
        case VK_PAD_RTRIGGER        : std::cout << "VK_PAD_R2" << ' ' << ks.Flags; break;
        case VK_PAD_DPAD_UP         : std::cout << "VK_PAD_UP" << ' ' << ks.Flags; break;
        case VK_PAD_DPAD_DOWN       : std::cout << "VK_PAD_DOWN" << ' ' << ks.Flags; break;
        case VK_PAD_DPAD_LEFT       : std::cout << "VK_PAD_LEFT" << ' ' << ks.Flags; break;
        case VK_PAD_DPAD_RIGHT      : std::cout << "VK_PAD_RIGHT" << ' ' << ks.Flags; break;
        case VK_PAD_START           : std::cout << "VK_PAD_START" << ' ' << ks.Flags; break;
        case VK_PAD_BACK            : std::cout << "VK_PAD_BACK" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_PRESS    : std::cout << "VK_PAD_LT" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_PRESS    : std::cout << "VK_PAD_RT" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_UP       : std::cout << "VK_PAD_LUP" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_DOWN     : std::cout << "VK_PAD_LDOWN" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_RIGHT    : std::cout << "VK_PAD_LRIGHT" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_LEFT     : std::cout << "VK_PAD_LLEFT" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_UPLEFT   : std::cout << "VK_PAD_LUPLEFT" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_UPRIGHT  : std::cout << "VK_PAD_LUPRIGHT" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_DOWNRIGHT: std::cout << "VK_PAD_LDOWNRIGHT" << ' ' << ks.Flags; break;
        case VK_PAD_LTHUMB_DOWNLEFT : std::cout << "VK_PAD_LDOWNLEFT" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_UP       : std::cout << "VK_PAD_RUP" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_DOWN     : std::cout << "VK_PAD_RDOWN" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_RIGHT    : std::cout << "VK_PAD_RRIGHT" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_LEFT     : std::cout << "VK_PAD_RLEFT" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_UPLEFT   : std::cout << "VK_PAD_RUPLEFT" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_UPRIGHT  : std::cout << "VK_PAD_RUPRIGHT" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_DOWNRIGHT: std::cout << "VK_PAD_RDOWNRIGHT" << ' ' << ks.Flags; break;
        case VK_PAD_RTHUMB_DOWNLEFT : std::cout << "VK_PAD_RDOWNLEFT" << ' ' << ks.Flags; break;
    }
    std::cout << std::endl;
}

int main(int argc, char* argv[])
{
    XINPUT_STATE states[16] = {};
    XINPUT_STATE oldStates[16] = {};
    XINPUT_VIBRATION vib;
    XINPUT_CAPABILITIES caps;

    vib.wLeftMotorSpeed = 65535;
    vib.wRightMotorSpeed = 65535;
    
    WCHAR str1[64], str2[64];
    UINT size1 = 64, size2 = 64;

    while (1)
    {
        for (int i = 0; i < XInputGetMaxController(); ++i)
        {
            if (XInputGetState(i, &states[i]) == ERROR_SUCCESS)
            {
                if (memcmp(&states[i], &oldStates[i], sizeof(XINPUT_STATE)) != 0)
                {
                    std::cout << "Controller: " << i << std::endl;
                    print_state(states[i]);
                    oldStates[i] = states[i];
                }
            }
        }
    }

    return 0;
}
