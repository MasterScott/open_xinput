=== Open source xinput library ===

This should help you manage more than 4 xinput gamepads.

And example can be found in my gamepad library here: https://gitlab.com/Nemirtingas/gamepad/-/blob/master/gamepad.cpp

Look for the XInput_Device, especially the:
  - Gamepad initialization function: get_gamepad_infos()
  - Gamepad data retrieval: get_gamepad_data()
  - Gamepad vibration: send_gamepad_vibration()
  - The XInput driver communication: DeviceInOutIo, DeviceInIo, DeviceOutIo, DeviceIo