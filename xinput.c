/* Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the xinput open-source project.
 *
 * xinput open-source project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * xinput open-source project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with xinput open-source project.  If not, see <https://www.gnu.org/licenses/>
 */


#include "xinputinternal.h"

static BOOLEAN s_Initialized = FALSE;
static BOOLEAN s_enabled = FALSE;
static DWORD s_LibModifier = 0;
static DWORD s_AllocatedDevicesCount;
static XInputInternalDevice** s_AllocatedDevices;
static PSP_DEVICE_INTERFACE_DETAIL_DATA_W s_pDeviceInterfaceDetailData;
static DWORD s_DeviceInterfaceDetailDataSize;
static DWORD s_LastTickCount = 0;

static XINPUT_STATE s_NeutralInputState = { 0 };

static XINPUT_CAPABILITIES s_XBoxCapabilities = {
    XINPUT_DEVTYPE_GAMEPAD,
    XINPUT_DEVSUBTYPE_GAMEPAD,
    XINPUT_CAPS_VOICE_SUPPORTED,
    {
        XINPUT_BUTTON_MASK_WITHOUT_GUIDE, 1, 1, 1, 1, 1, 1
    },
    {
        1, 1
    }
};

static RTL_CRITICAL_SECTION s_XInputCriticalSection;

HRESULT XInputSetStateDisabled(XInputInternalDevice* pDevice, void* param, ULONGLONG);
HRESULT XInputGetStateDisabled(XInputInternalDevice* pDevice, void* param, ULONGLONG);
HRESULT XInputSetStateEnabled(XInputInternalDevice* pDevice, void* param, ULONGLONG);
HRESULT XInputGetStateEnabled(XInputInternalDevice* pDevice, void* param, ULONGLONG);

static action_func_t pXInputSetStateFunc = XInputSetStateDisabled;
static action_func_t pXInputGetStateFunc = XInputGetStateDisabled;

HRESULT GetOrDiscoverDevice(DWORD dwUserIndex, XInputInternalDevice** ppDevice, BYTE retry_count);
HRESULT GetDeviceAudioIds(XInputInternalDevice* pDevice, void* param, ULONGLONG arg);

LPVOID XInputAlloc(SIZE_T size)
{
    return LocalAlloc(LPTR, size);
}

void XInputFree(LPVOID data)
{
    if (data != NULL)
        LocalFree(data);
}

HRESULT AllocateDevices()
{
    s_AllocatedDevicesCount = 0;
    s_AllocatedDevices = (XInputInternalDevice**)XInputAlloc(sizeof(*s_AllocatedDevices) * XUSER_MAX_COUNT);
    if (!s_AllocatedDevices)
        return E_FAIL;

    s_AllocatedDevicesCount = XUSER_MAX_COUNT;

    return NOERROR;
}

void InternalEnable(BOOLEAN enable)
{
    XInputInternalDevice* pDevice;
    XINPUT_STATE* state = NULL;

    if (s_enabled != enable)
    {
        if (enable == TRUE)
        {
            pXInputSetStateFunc = XInputSetStateEnabled;
            pXInputGetStateFunc = XInputGetStateEnabled;
        }
        else
        {
            pXInputSetStateFunc = XInputSetStateDisabled;
            pXInputGetStateFunc = XInputGetStateDisabled;
        }
        s_enabled = enable;

        for (int i = 0; i < XUSER_MAX_COUNT; ++i)
        {
            pDevice = NULL;
            if (GetOrDiscoverDevice(i, &pDevice, 0) >= 0)
            {
                if (pDevice)
                    pXInputSetStateFunc(pDevice, (void**)&state, TRUE);
            }
        }
    }

}

BOOLEAN InitXInputLibrary()
{
    BOOLEAN ret = FALSE;
    InitializeCriticalSection(&s_XInputCriticalSection);
    s_LibModifier = 5;

    if (AllocateDevices() >= 0)
    {
        s_pDeviceInterfaceDetailData = NULL;
        s_DeviceInterfaceDetailDataSize = 0;

        InternalEnable(TRUE);
        s_Initialized = TRUE;
        ret = TRUE;
    }

    return ret;
}

BOOLEAN ModifyLibrary(DWORD dwReason, LPVOID param)
{
    switch (dwReason)
    {
        case 0xBEEF0000: break;
        case 0xBEEF0001:
            if (param != NULL)
                s_LibModifier |= 1;
            else
                s_LibModifier &= ~1;
            break;
        case 0xBEEF0002:
            if (param != NULL)
                s_LibModifier |= 2;
            else
                s_LibModifier &= ~2;
            break;
        case 0xBEEF0003:
            if (param != NULL)
                s_LibModifier |= 4;
            else
                s_LibModifier &= ~4;
            break;

        default: return FALSE; 
    }

    return TRUE;
}

BOOLEAN HookLibraryFunc(DWORD dwReason, LPVOID param)
{
    switch (dwReason)
    {
        case 0xBAAD0000: break; // Nothing
        case 0xBAAD0001: // DeviceIoControl Set
        case 0xBAAD0002: // DeviceIoControl Reset
        case 0xBAAD0003: // SetupDiGetClassDevsW Set
        case 0xBAAD0004: // SetupDiGetClassDevsW Reset
        case 0xBAAD0005: // SetupDiEnumDeviceInterfaces Set
        case 0xBAAD0006: // SetupDiEnumDeviceInterfaces Reset
        case 0xBAAD0007: // SetupDiGetDeviceInterfaceDetailW Set
        case 0xBAAD0008: // SetupDiGetDeviceInterfaceDetailW Reset
        case 0xBAAD0009: // SetupDiDestroyDeviceInfoList Set
        case 0xBAAD000A: // SetupDiDestroyDeviceInfoList Reset
            break;

        default: return FALSE;
    }
    return TRUE;
}

BOOL CALLBACK direct_sound_enumerate_callback(PDSPROPERTY_DIRECTSOUNDDEVICE_DESCRIPTION_W_DATA data, LPVOID _param)
{
    dsound_func_param_t* param = (dsound_func_param_t*)_param;

    if (param == NULL)
        return FALSE;

    if (data->Type == DIRECTSOUNDDEVICE_TYPE_WDM)
    {
        size_t device_len = wcslen(param->device_path);
        size_t interface_len = wcslen(data->Interface);
        if (interface_len >= device_len)
        {
            if (wcsncmp(data->Interface, param->device_path, device_len) == 0)
            {
                if (data->DataFlow == DIRECTSOUNDDEVICE_DATAFLOW_CAPTURE)
                {
                    memcpy(param->microphone_guid, &data->DeviceId, sizeof(GUID));
                }
                else if (data->DataFlow == DIRECTSOUNDDEVICE_DATAFLOW_RENDER)
                {
                    memcpy(param->headphone_guid, &data->DeviceId, sizeof(GUID));
                }
            }
        }
    }

    return TRUE;
}

HRESULT DeviceIo(HANDLE hDev, DWORD ioControlCode, LPVOID inBuff, DWORD inBuffSize, LPVOID outBuff, DWORD outBuffSize, LPOVERLAPPED lpOverlapped)
{
    DWORD error;
    DWORD BytesReturned;

    BytesReturned = 0;
    //if (FuncDeviceIoControl)
    //    error = FuncDeviceIoControl(hDev, ioControlCode, inBuff, inBuffSize, outBuff, outBuffSize, &lpBytesReturned, lpOverlapped);
    //else
    error = DeviceIoControl(hDev, ioControlCode, inBuff, inBuffSize, outBuff, outBuffSize, &BytesReturned, lpOverlapped);
    if (error == TRUE)
        return NO_ERROR;

    error = GetLastError();
    if (error == ERROR_IO_PENDING)
    {
        if (lpOverlapped != NULL)
            return E_PENDING;
    }
    else if (error == ERROR_BAD_COMMAND)
        return E_HANDLE;

    return E_FAIL;
}

HDEVINFO DiGetClassDevW(const GUID* ClassGuid, PCWSTR Enumerator, HWND hwndParent, DWORD Flags)
{
    HDEVINFO result;

    //if (FuncSetupDiGetClassDevsW)
    //    result = pSetupDiGetClassDevsW(ClassGuid, Enumerator, hwndParent, Flags);
    //else
    result = SetupDiGetClassDevsW(ClassGuid, Enumerator, hwndParent, Flags);
    return result;
}

HRESULT GetClassDev(HDEVINFO* phDev)
{
    *phDev = DiGetClassDevW(&XUSB_INTERFACE_CLASS_GUID, 0, 0, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
    if (*phDev == INVALID_HANDLE_VALUE)
        return E_FAIL;

    return 0;
}

int DestroyDiDeviceInfoList(HDEVINFO hDev)
{
    //if (pSetupDiDestroyDeviceInfoList)
    //    return pSetupDiDestroyDeviceInfoList(hDev);

    return SetupDiDestroyDeviceInfoList(hDev);
}

DWORD DiEnumDeviceInterfaces(HDEVINFO hDev, PSP_DEVINFO_DATA pDevInfoData, const GUID* pGuid, DWORD memberIndex, PSP_DEVICE_INTERFACE_DATA pDevIface)
{
    //if (FuncSetupDiEnumDeviceInterfaces)
    //    return FuncSetupDiEnumDeviceInterfaces(hDev, pDevInfoData, pGuid, memberIndex, pDevIface);

    return SetupDiEnumDeviceInterfaces(hDev, pDevInfoData, pGuid, memberIndex, pDevIface);
}

DWORD GetDevInterface(HDEVINFO hDev, DWORD memberIndex, PSP_DEVICE_INTERFACE_DATA pDevIface)
{
    memset(pDevIface, 0, sizeof(*pDevIface));
    pDevIface->cbSize = sizeof(*pDevIface);
    if (DiEnumDeviceInterfaces(hDev, NULL, &XUSB_INTERFACE_CLASS_GUID, memberIndex, pDevIface))
        return NOERROR;

    return E_FAIL;
}

DWORD GetDeviceInterfaceDetailW(HDEVINFO hDev, PSP_DEVICE_INTERFACE_DATA pDevIfaceData, PSP_DEVICE_INTERFACE_DETAIL_DATA_W pDeviceInfoData, DWORD deviceInfoDataSize, PDWORD RequiredSize, PSP_DEVINFO_DATA DeviceInfoData)
{
    //if (pSetupDiGetDeviceInterfaceDetailW)
    //    return pSetupDiGetDeviceInterfaceDetailW)(hDev, pDevIfaceData, pDeviceInfoData, deviceInfoDataSize, RequiredSize, DeviceInfoData);

    return SetupDiGetDeviceInterfaceDetailW(hDev, pDevIfaceData, pDeviceInfoData, deviceInfoDataSize, RequiredSize, DeviceInfoData);
}

HRESULT GetGamepadDetail(HDEVINFO hDev, PSP_DEVICE_INTERFACE_DATA pDevInfosData, PSP_DEVICE_INTERFACE_DETAIL_DATA_W* pDevIfaceDetailData)
{
    DWORD uBytes;
    DWORD res;

    uBytes = s_DeviceInterfaceDetailDataSize;
    res = 0;
    *pDevIfaceDetailData = s_pDeviceInterfaceDetailData;
    if (*pDevIfaceDetailData)
    {
        memset(*pDevIfaceDetailData, 0, s_DeviceInterfaceDetailDataSize);
        (*pDevIfaceDetailData)->cbSize = sizeof(**pDevIfaceDetailData);
    }
    res = GetDeviceInterfaceDetailW(hDev, pDevInfosData, *pDevIfaceDetailData,
        s_DeviceInterfaceDetailDataSize, &uBytes, NULL);
    if (res)
        return NOERROR;

    if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
    {
        *pDevIfaceDetailData = NULL;
        return E_FAIL;
    }

    *pDevIfaceDetailData = NULL;
    if (s_pDeviceInterfaceDetailData)
    {
        XInputFree(s_pDeviceInterfaceDetailData);
        s_pDeviceInterfaceDetailData = NULL;
        s_DeviceInterfaceDetailDataSize = 0;
    }

    s_pDeviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA_W)XInputAlloc(uBytes);
    if (!s_pDeviceInterfaceDetailData)
        return E_OUTOFMEMORY;

    s_DeviceInterfaceDetailDataSize = uBytes;
    *pDevIfaceDetailData = s_pDeviceInterfaceDetailData;
    (*pDevIfaceDetailData)->cbSize = sizeof(**pDevIfaceDetailData);
    res = GetDeviceInterfaceDetailW(hDev, pDevInfosData, *pDevIfaceDetailData, s_DeviceInterfaceDetailDataSize, NULL, NULL);
    if (res)
        return NOERROR;

    *pDevIfaceDetailData = NULL;
    return E_FAIL;
}

XInputInternalDevice* CreateDevice(HANDLE hDev, LPWSTR str)
{
    XInputInternalDevice* pInternalDevice;
    HANDLE hCurrentProcess;

    pInternalDevice = (XInputInternalDevice*)XInputAlloc(sizeof(XInputInternalDevice));
    if (pInternalDevice)
    {
        memset(pInternalDevice, 0, sizeof(XInputInternalDevice));
        pInternalDevice->hDevice = INVALID_HANDLE_VALUE;
        pInternalDevice->handle2 = INVALID_HANDLE_VALUE;

        hCurrentProcess = GetCurrentProcess();
        if (DuplicateHandle(hCurrentProcess, hDev, hCurrentProcess, &pInternalDevice->hDevice, 0, 0, 2u))
        {
            pInternalDevice->strlen = wcslen(str) + 1;
            pInternalDevice->str = (LPWSTR)XInputAlloc(sizeof(WCHAR) * pInternalDevice->strlen);
            if (pInternalDevice->str)
            {
                if (wcscpy_s(pInternalDevice->str, pInternalDevice->strlen, str) >= 0)
                    return pInternalDevice;

            }
        }
    }
    if (pInternalDevice)
    {
        if (pInternalDevice->str)
            XInputFree(pInternalDevice->str);
        if (pInternalDevice->hDevice != INVALID_HANDLE_VALUE)
            CloseHandle(pInternalDevice->hDevice);
        XInputFree(pInternalDevice);
    }
    return NULL;
}

void DeleteDevice(XInputInternalDevice* pInternalDevice)
{
    if (pInternalDevice)
    {
        if (pInternalDevice->handle2 != INVALID_HANDLE_VALUE)
            CloseHandle(pInternalDevice->handle2);
        if (pInternalDevice->hDevice != INVALID_HANDLE_VALUE)
            CloseHandle(pInternalDevice->hDevice);
        if (pInternalDevice->str)
            XInputFree(pInternalDevice->str);
        XInputFree(pInternalDevice);
    }
}

HRESULT MemCopy(PVOID dest, const PVOID src, SIZE_T count)
{
    memcpy(dest, src, count);
    return ERROR_SUCCESS;
}

WORD JoystickToKey(WORD baseKey, SHORT x, SHORT y)
{
    if (y > 20000)
    {
        if (x < -20000)
            return baseKey + 4;
        if (x > 20000)
            return baseKey + 5;

        return baseKey + 0;
    }

    if (y < -20000)
    {
        if (x > 20000)
            return baseKey + 6;
        if (x < -20000)
            return baseKey + 7;

        return baseKey + 1;
    }

    if (x > 20000)
        return baseKey + 2;
    if (x < -20000)
        return baseKey + 3;

    return 0;
}

void GetXBoxState(XInputInternalDevice* pDevice, buff_out_xbox_t* buff)
{
    pDevice->xinput_state.dwPacketNumber = buff->dwPacketNumber;
    pDevice->xinput_state.Gamepad.bLeftTrigger = buff->bLeftTrigger;
    pDevice->xinput_state.Gamepad.bRightTrigger = buff->bRightTrigger;
    pDevice->xinput_state.Gamepad.sThumbLX = buff->sThumbLX;
    pDevice->xinput_state.Gamepad.sThumbLY = buff->sThumbLY;
    pDevice->xinput_state.Gamepad.sThumbRX = buff->sThumbRX;
    pDevice->xinput_state.Gamepad.sThumbRY = buff->sThumbRY;
    pDevice->xinput_state.Gamepad.wButtons = buff->wButtons & XINPUT_BUTTON_MASK;
    if (buff->status == 1)
        pDevice->status |= STATUS_CONNECTED;
    else
        pDevice->status &= ~STATUS_CONNECTED;

    pDevice->input_id = buff->input_id;
}

void GetUnknownDevState(XInputInternalDevice* pDevice, buff_out_unknown_dev_t* buff)
{// Is it really for xbox one ?
    pDevice->xinput_state.dwPacketNumber = buff->dwPacketNumber;
    pDevice->xinput_state.Gamepad.bLeftTrigger = buff->bLeftTrigger;
    pDevice->xinput_state.Gamepad.bRightTrigger = buff->bRightTrigger;
    pDevice->xinput_state.Gamepad.sThumbLX = buff->sThumbLX;
    pDevice->xinput_state.Gamepad.sThumbLY = buff->sThumbLY;
    pDevice->xinput_state.Gamepad.sThumbRX = buff->sThumbRX;
    pDevice->xinput_state.Gamepad.sThumbRY = buff->sThumbRY;
    pDevice->xinput_state.Gamepad.wButtons = buff->wButtons;
    if (buff->status == 1)
        pDevice->status |= STATUS_CONNECTED;
    else
        pDevice->status &= ~STATUS_CONNECTED;

    pDevice->input_id = buff->input_id;
}

HRESULT GetDeviceState(XInputInternalDevice* pDevice)
{
    union
    {
        buff_in_t xboxInBuff;
        BYTE unknownDevInBuff;
    } in_buff;
    union
    {
        buff_out_unknown_dev_t unknownDevOutBuff;
        buff_out_xbox_t xboxOutBuff;
    } out_buff;

    DWORD inBuffSize;
    DWORD outBuffSize;
    HRESULT res;

    if (pDevice->type == 256)
    {// I don't know what device it is
        in_buff.unknownDevInBuff = pDevice->dwUserIndex;
        inBuffSize = sizeof(in_buff.unknownDevInBuff);
        outBuffSize = sizeof(out_buff.unknownDevOutBuff);
    }
    else
    {
        in_buff.xboxInBuff.field_0 = 257;
        in_buff.xboxInBuff.field_2 = pDevice->dwUserIndex;
        inBuffSize = sizeof(in_buff.xboxInBuff);
        outBuffSize = sizeof(out_buff.xboxOutBuff);
    }

    memset(&out_buff, 0, outBuffSize);

    res = DeviceIo(pDevice->hDevice, IOCTL_XINPUT_GET_GAMEPAD_STATE, &in_buff, inBuffSize, &out_buff, outBuffSize, NULL);
    if (res < 0)
        return res;

    if (pDevice->type == 256)
        GetUnknownDevState(pDevice, &out_buff.unknownDevOutBuff);
    else
        GetXBoxState(pDevice, &out_buff.xboxOutBuff);

    return NOERROR;
}

void GetXCapabilities(buff_capabilities_t* pBufCapabilities, XINPUT_CAPABILITIES* pCapabilities)
{
    pCapabilities->Type = pBufCapabilities->Type;
    pCapabilities->SubType = pBufCapabilities->SubType;
    pCapabilities->Flags = XINPUT_CAPS_VOICE_SUPPORTED;
    pCapabilities->Gamepad.wButtons = pBufCapabilities->wButtons;
    pCapabilities->Gamepad.bLeftTrigger = pBufCapabilities->bLeftTrigger;
    pCapabilities->Gamepad.bRightTrigger = pBufCapabilities->bRightTrigger;
    pCapabilities->Gamepad.sThumbLX = pBufCapabilities->sThumbLX;
    pCapabilities->Gamepad.sThumbLY = pBufCapabilities->sThumbLY;
    pCapabilities->Gamepad.sThumbRX = pBufCapabilities->sThumbRX;
    pCapabilities->Gamepad.sThumbRY = pBufCapabilities->sThumbRY;
    pCapabilities->Vibration.wLeftMotorSpeed = pBufCapabilities->wLeftMotorSpeed;
    pCapabilities->Vibration.wRightMotorSpeed = pBufCapabilities->wRightMotorSpeed;
    pCapabilities->Gamepad.wButtons &= XINPUT_BUTTON_MASK;
}

int SetVibration(XINPUT_VIBRATION* s1, XINPUT_VIBRATION* s2)
{
    *s1 = *s2;
    return 0;
}

HRESULT SendVibration(XInputInternalDevice* pDevice)
{
    buff_vibration_t buff;

    buff.field_0 = pDevice->dwUserIndex;
    buff.field_1 = 0;
    buff.left_speed = pDevice->xinput_vibration.wLeftMotorSpeed / 256;
    buff.right_speed = pDevice->xinput_vibration.wRightMotorSpeed / 256;
    buff.field_4 = 2;
    return DeviceIo(pDevice->hDevice, IOCTL_XINPUT_SET_GAMEPAD_STATE, &buff, sizeof(buff), NULL, 0, NULL);
}

HRESULT ReadCapabilites(XInputInternalDevice* pInternalDevice, XINPUT_CAPABILITIES* pCapabilities)
{
    HRESULT res;
    buff_capabilities_t outBuff;
    buff_in_t inBuff;

    if (pInternalDevice->type == 256)
        return MemCopy(pCapabilities, &s_XBoxCapabilities, sizeof(*pCapabilities));

    inBuff.field_0 = 257;
    inBuff.field_2 = pInternalDevice->dwUserIndex;

    memset(&outBuff, 0, sizeof(outBuff));
    res = DeviceIo(pInternalDevice->hDevice, IOCTL_XINPUT_GET_CAPABILITIES, &inBuff, sizeof(inBuff), &outBuff, sizeof(outBuff), NULL);
    if (res >= 0)
        GetXCapabilities(&outBuff, pCapabilities);

    return res;
}

HRESULT ReadDeviceInputId(XInputInternalDevice* pDevice, device_ids_t* pIds)
{
    HRESULT result;
    buff_in_t in_buff;
    buff_ids_t out_buff;

    if (pDevice->type < 258)
        return E_FAIL;

    in_buff.field_0 = 258;
    in_buff.field_2 = pDevice->dwUserIndex;
    result = DeviceIo(pDevice->hDevice, IOCTL_XINPUT_GET_AUDIO_INFORMATION, &in_buff, sizeof(in_buff), &out_buff, sizeof(out_buff), NULL);
    if (result >= 0)
        *pIds = out_buff.ids;

    return result;
}

void ResetBatteryInfo(battery_info* pInfos)
{
    pInfos->deviceInterfaceDetail = 0;
    pInfos->deviceInterfaceDetailSize = 0;
    pInfos->hDev = INVALID_HANDLE_VALUE;
    pInfos->memberIndex = 0;
}

BOOLEAN InitBatteryInfo(battery_info* pInfos)
{
    if (pInfos->hDev != INVALID_HANDLE_VALUE)
    {
        DestroyDiDeviceInfoList(pInfos->hDev);
        pInfos->hDev = INVALID_HANDLE_VALUE;
    }
    pInfos->memberIndex = 0;
    if (pInfos->deviceInterfaceDetail == NULL)
    {
        pInfos->deviceInterfaceDetailSize = (sizeof(DWORD) + MAX_PATH * sizeof(WCHAR));
        pInfos->deviceInterfaceDetail = (PSP_DEVICE_INTERFACE_DETAIL_DATA_W)XInputAlloc(pInfos->deviceInterfaceDetailSize);

        if (pInfos->deviceInterfaceDetail == NULL)
        {
            pInfos->deviceInterfaceDetailSize = 0;
            return FALSE;
        }
    }
    
    pInfos->hDev = DiGetClassDevW(&XUSB_INTERFACE_CLASS_GUID, NULL, NULL, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);

    return pInfos->hDev != INVALID_HANDLE_VALUE;
}

void DeinitBatteryInfo(battery_info* pInfos)
{
    if (pInfos->hDev != INVALID_HANDLE_VALUE)
    {
        DestroyDiDeviceInfoList(pInfos->hDev);
        pInfos->hDev = INVALID_HANDLE_VALUE;
        pInfos->memberIndex = 0;
    }
    if (pInfos->deviceInterfaceDetail != NULL)
    {
        XInputFree(pInfos->deviceInterfaceDetail);
        pInfos->deviceInterfaceDetail = NULL;
        pInfos->deviceInterfaceDetailSize = 0;
    }
}

BOOLEAN OpenBatteryDevice(battery_info* pInfos, HANDLE* pHandle)
{
    SP_DEVICE_INTERFACE_DATA DeviceInterfaceData;
    DWORD RequiredSize = 0;

    if (pInfos->hDev == NULL)
        return FALSE;

    memset(&DeviceInterfaceData, 0, sizeof(DeviceInterfaceData));
    DeviceInterfaceData.cbSize = sizeof(DeviceInterfaceData);

    if (!DiEnumDeviceInterfaces(pInfos->hDev, NULL, &XUSB_INTERFACE_CLASS_GUID, pInfos->memberIndex, &DeviceInterfaceData))
        return FALSE;

    ++pInfos->memberIndex;

    pInfos->deviceInterfaceDetail->cbSize = sizeof(pInfos->deviceInterfaceDetail);

    if (GetDeviceInterfaceDetailW(pInfos->hDev, &DeviceInterfaceData, pInfos->deviceInterfaceDetail, pInfos->deviceInterfaceDetailSize, &RequiredSize, NULL) != 0)
    {
        if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            return FALSE;

        XInputFree(pInfos->deviceInterfaceDetail);
        pInfos->deviceInterfaceDetailSize = RequiredSize;
        pInfos->deviceInterfaceDetail = (PSP_DEVICE_INTERFACE_DETAIL_DATA_W)XInputAlloc(RequiredSize);
        if (pInfos->deviceInterfaceDetail == NULL)
        {
            pInfos->deviceInterfaceDetailSize = 0;
            return FALSE;
        }

        pInfos->deviceInterfaceDetail->cbSize = sizeof(pInfos->deviceInterfaceDetail);

        if (GetDeviceInterfaceDetailW(pInfos->hDev, &DeviceInterfaceData, pInfos->deviceInterfaceDetail, pInfos->deviceInterfaceDetailSize, NULL, NULL) != 0)
            return 0;
    }

    pInfos->hDev = CreateFileW(pInfos->deviceInterfaceDetail->DevicePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    return TRUE;
}

BOOLEAN InitInternalDeviceFromHANDLE(HANDLE hDev, XInputInternalDevice* pDevice)
{
    buff_init_t init_buff;

    if (hDev == INVALID_HANDLE_VALUE || pDevice == NULL || DeviceIo(hDev, IOCTL_XINPUT_GET_INFORMATION, NULL, 0, &init_buff, sizeof(init_buff), 0) < 0 || init_buff.field_4 < 0)
        return FALSE;
    memset(pDevice, 0, sizeof(*pDevice));
    pDevice->handle2 = INVALID_HANDLE_VALUE;
    pDevice->type = init_buff.type;
    pDevice->vendor_id = init_buff.vendor_id;
    pDevice->hDevice = hDev;
    pDevice->status = STATUS_CONNECTED;
    pDevice->product_id = init_buff.product_id;
    return TRUE;
}

HRESULT GetKeystroke(XInputInternalDevice* pDevice, DWORD dwUserIndex, DWORD reserved, XINPUT_KEYSTROKE* pKeystroke)
{
    static DWORD key_code;
    static BOOL key_down;
    static DWORD down_tick_start;

    XINPUT_STATE state;
    XINPUT_STATE* pState = &state;

    int current_key;
    WORD key_bit;
    WORD tmp_key;
    WORD key;
    DWORD current_tick_count;
    DWORD repeat_tick_count;

    reserved = 1;
    if (pXInputGetStateFunc(pDevice, &pState, reserved) < 0)
        return ERROR_EMPTY;

    state.Gamepad.wButtons &= XINPUT_BUTTON_MASK_WITHOUT_GUIDE;

    pKeystroke->UserIndex = (BYTE)dwUserIndex;
    pKeystroke->Unicode = 0;

    key = 0;

    tmp_key = JoystickToKey(VK_PAD_LTHUMB_UP, state.Gamepad.sThumbLX, state.Gamepad.sThumbLY);
    pKeystroke->VirtualKey = tmp_key;

    if (pDevice->left_stick_key != tmp_key)
    {
        if (pDevice->left_stick_key)
        {
            pKeystroke->VirtualKey = pDevice->left_stick_key;
            pKeystroke->Flags = XINPUT_KEYSTROKE_KEYUP;
            key_code = 0;
            pDevice->left_stick_key = 0;
            return NOERROR;
        }
        key = tmp_key;
        pDevice->left_stick_key = tmp_key;
    }

    tmp_key = JoystickToKey(VK_PAD_RTHUMB_UP, state.Gamepad.sThumbRX, state.Gamepad.sThumbRY);
    if (tmp_key)
        pKeystroke->VirtualKey = tmp_key;

    if (pDevice->right_stick_key != tmp_key)
    {
        if (pDevice->right_stick_key)
        {
            pKeystroke->VirtualKey = pDevice->right_stick_key;
            pKeystroke->Flags = XINPUT_KEYSTROKE_KEYUP;
            key_code = 0;
            pDevice->right_stick_key = 0;
            return NOERROR;
        }
        key = tmp_key;
        pDevice->right_stick_key = tmp_key;
    }

    current_key = 0;
    if (state.Gamepad.bLeftTrigger > 30)
        current_key = 1;
    if (state.Gamepad.bRightTrigger > 30)
        current_key = 2;

    tmp_key = VK_PAD_LTRIGGER;

    for (int i = 0; i < 2; ++i)
    {
        key_bit = 1 << i;
        if (pDevice->trigger_key & key_bit)
        {
            pKeystroke->VirtualKey = tmp_key;
            if (!(current_key & key_bit))
            {
                pDevice->trigger_key &= ~key_bit;
                key_code = 0;
                pKeystroke->Flags = XINPUT_KEYSTROKE_KEYUP;
                return NOERROR;
            }
        }
        else if (current_key & key_bit)
        {
            pDevice->trigger_key |= key_bit;
            key = tmp_key;
        }
        ++tmp_key;
    }

    current_key = state.Gamepad.wButtons;

    for (int i = 0; i < 16; ++i)
    {
        switch (i)
        {
        case 0: tmp_key = VK_PAD_DPAD_UP; break;
        case 8: tmp_key = VK_PAD_LSHOULDER; break;
        case 9: tmp_key = VK_PAD_RSHOULDER; break;
        case 12: tmp_key = VK_PAD_A; break;
        }
        key_bit = 1 << i;
        if (pDevice->button_keys & key_bit)
        {
            pKeystroke->VirtualKey = tmp_key;
            if (!(current_key & key_bit))
            {
                pDevice->button_keys &= ~key_bit;
                pKeystroke->Flags = XINPUT_KEYSTROKE_KEYUP;
                key_code = 0;
                return 0;
            }
        }
        else if (current_key & key_bit)
        {
            pDevice->button_keys |= key_bit;
            key = tmp_key;
        }
        ++tmp_key;
    }

    current_tick_count = GetTickCount();
    if (key)
    {
        pKeystroke->VirtualKey = key;
        pKeystroke->Flags = XINPUT_KEYSTROKE_KEYDOWN;
        key_code = key;
        down_tick_start = current_tick_count;
        key_down = TRUE;
    }
    else if (pKeystroke->VirtualKey == key_code)
    {
        if (key_down == TRUE)// If we just pressed the key, wait for 400ms
            repeat_tick_count = 400;
        else// If its a repeat, send every 100ms
            repeat_tick_count = 100;

        if ((current_tick_count - down_tick_start) < repeat_tick_count)
        {
            pKeystroke->VirtualKey = 0;
        }
        else
        {
            pKeystroke->Flags = XINPUT_KEYSTROKE_REPEAT;
            down_tick_start = current_tick_count;
            key_down = FALSE;
        }
    }
    else
    {
        pKeystroke->VirtualKey = 0;
    }

    if (!pKeystroke->VirtualKey)
        return ERROR_EMPTY;

    return ERROR_SUCCESS;
}

HRESULT XInputSetStateDisabled(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    HRESULT res;
    XInputInternalDevice neutralVibration;
    XINPUT_VIBRATION** ppState = (XINPUT_VIBRATION**)param;

    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    if (*ppState)
    {
        res = MemCopy(&pDevice->xinput_vibration, *ppState, sizeof(**ppState));
    }
    else
    {
        memcpy(&neutralVibration, pDevice, sizeof(neutralVibration));
        neutralVibration.xinput_vibration.wLeftMotorSpeed = 0;
        neutralVibration.xinput_vibration.wRightMotorSpeed = 0;
        res = SendVibration(&neutralVibration);
    }
    return res;
}

HRESULT XInputGetStateDisabled(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    HRESULT result;
    XINPUT_STATE** ppState = (XINPUT_STATE**)param;

    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    result = MemCopy(*ppState, &s_NeutralInputState, sizeof(**ppState));
    if (result >= 0)
        (*ppState)->dwPacketNumber = pDevice->xinput_state.dwPacketNumber + 1;

    return result;
}

HRESULT XInputSetStateEnabled(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    HRESULT res;
    XINPUT_VIBRATION* pVibration = *(XINPUT_VIBRATION**)param;

    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    if (pVibration != NULL && (res = SetVibration(&pDevice->xinput_vibration, pVibration)) < 0)
        return res;

    return SendVibration(pDevice);
}

HRESULT XInputGetStateEnabled(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    HRESULT result;
    XINPUT_STATE** ppState = (XINPUT_STATE**)param;

    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    result = GetDeviceState(pDevice);
    if (result >= 0)
        result = MemCopy(*ppState, &pDevice->xinput_state, sizeof(**ppState));

    return result;
}

HRESULT XInputGetCapabilitiesEnabled(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    XINPUT_CAPABILITIES** ppState = (XINPUT_CAPABILITIES**)param;

    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    return ReadCapabilites(pDevice, *ppState);
}

HRESULT XInputGetKeystrokeEnabled(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    internal_keystroke_arg_t* keystroke_arg = (internal_keystroke_arg_t*)param;

    *keystroke_arg->result = GetKeystroke(pDevice, 0, *keystroke_arg->reserved, keystroke_arg->xinput_keystroke);

    return NOERROR;
}

HRESULT GetBatteryInfo(XInputInternalDevice* pDevice, BYTE devType, XINPUT_BATTERY_INFORMATION* pBatteryInformation)
{
    int res = 0;
    buff_in_battery_t buff_in_batt;
    buff_out_battery_t buff_out_batt;

    if (pDevice->type >= 258)
    {
        buff_in_batt.type = 258;
        buff_in_batt.field_2 = pDevice->dwUserIndex;
        buff_in_batt.devType = devType;
        memset(&buff_out_batt, 0, sizeof(buff_out_batt));

        res = DeviceIo(pDevice->hDevice, IOCTL_XINPUT_GET_BATTERY_INFORMATION, &buff_in_batt, sizeof(buff_in_batt), &buff_out_batt, sizeof(buff_out_batt), NULL);
        if (res < 0)
            return res;

        pBatteryInformation->BatteryLevel = buff_out_batt.BatteryLevel;
        pBatteryInformation->BatteryType = buff_out_batt.BatteryType;
    }
    else
    {
        GUID* guids[2];
        GUID headphone_guid;
        GUID microphone_guid;
        int v8;

        XINPUT_BATTERY_INFORMATION battInfo;
        battInfo.BatteryType = BATTERY_TYPE_WIRED;
        battInfo.BatteryLevel = BATTERY_LEVEL_FULL;
        if (devType == BATTERY_DEVTYPE_HEADSET)
        {
            guids[0] = &headphone_guid;
            guids[1] = &microphone_guid;
            v8 = GetDeviceAudioIds(pDevice, guids, 2);
            if (v8 < 0 || memcmp(&headphone_guid, &null_guid, sizeof(GUID)) == 0)
            {
                battInfo.BatteryType = 0;
                battInfo.BatteryLevel = 0;
            }
        }
        res = MemCopy(pBatteryInformation, &battInfo, sizeof(XINPUT_BATTERY_INFORMATION));
    }
    return res;
}

HRESULT GetDeviceAudioIds(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    DWORD vendor_id;
    DWORD product_id;
    DWORD input_id;
    device_ids_t buff;
    HMODULE hdsound;

    dsound_func_param_t dsound_param;

    GUID** pGuids = (GUID**)param;

    memset(pGuids[0], 0, sizeof(GUID));
    memset(pGuids[1], 0, sizeof(GUID));

    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    vendor_id = pDevice->vendor_id;
    product_id = pDevice->product_id;

    if (pDevice->type >= 258)
    {
        if (ReadDeviceInputId(pDevice, &buff) < 0)
            return E_FAIL;

        vendor_id = buff.vendor_id;
        product_id = buff.product_id;
        input_id = buff.input_id;

        if (pDevice->input_id == 255)
            return E_FAIL;
    }
    else
    {
        int res = GetDeviceState(pDevice);
        if (res < 0)
            return res;
        if (!(pDevice->status & STATUS_CONNECTED))
            return E_FAIL;

        input_id = pDevice->input_id;
        if (input_id == 255)
            return 0;
    }

    HRESULT res;
    hdsound = LoadLibraryW(L"dsound.dll");
    if (hdsound != INVALID_HANDLE_VALUE && hdsound != NULL)
    {
        HRESULT(WINAPI * pDllGetClassObject)(const GUID*, const GUID*, LPVOID*) = (HRESULT(WINAPI*)(const GUID*, const GUID*, LPVOID*))GetProcAddress(hdsound, "DllGetClassObject");

        if (pDllGetClassObject != NULL)
        {
            WCHAR device_path[64];

            if (swprintf_s(device_path, sizeof(device_path) / sizeof(*device_path), L"\\\\?\\USB#VID_%04X&PID_%04X&IA_%02X", vendor_id, product_id, input_id) >= 0)
            {
                IClassFactory* pClassFactory;
                res = pDllGetClassObject(&CLSID_DirectSoundPrivate, &IID_IClassFactory, (PVOID*)&pClassFactory);
                if (res >= 0)
                {
                    IKsPropertySet* pIKsPropertySet;
                    res = pClassFactory->lpVtbl->CreateInstance(pClassFactory, NULL, &IID_IKsPropertySet, (PVOID*)&pIKsPropertySet);
                    pClassFactory->lpVtbl->Release(pClassFactory);
                    pClassFactory = NULL;
                    if (res >= 0)
                    {
                        DSPROPERTY_DIRECTSOUNDDEVICE_ENUMERATE_W_DATA param;

                        dsound_param.device_path = device_path;
                        dsound_param.headphone_guid = pGuids[0];
                        dsound_param.microphone_guid = pGuids[1];

                        param.Callback = &direct_sound_enumerate_callback;
                        param.Context = &dsound_param;
                        res = pIKsPropertySet->lpVtbl->Get(pIKsPropertySet, &DSPROPSETID_DirectSoundDevice, 8, NULL, 0, (LPVOID)&param, sizeof(param), 0);

                        pIKsPropertySet->lpVtbl->Release(pIKsPropertySet);
                        pIKsPropertySet = NULL;
                    }
                }
            }
            else
            {
                res = E_FAIL;
            }
        }
        else
        {
            res = E_FAIL;
        }

        FreeLibrary(hdsound);
    }
    else
    {
        res = E_FAIL;
    }

    return res;
}

HRESULT GetDeviceBatteryInfo(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    internal_battery_info_arg_t* battery_arg = (internal_battery_info_arg_t*)param;

    return GetBatteryInfo(pDevice, *battery_arg->devType, battery_arg->pBatteryInformation);
}

HRESULT PowerOffController(XInputInternalDevice* pDevice)
{
    buff_in_t buff;

    if (pDevice->type < 258)
        return E_FAIL;

    buff.field_0 = 258;
    buff.field_2 = pDevice->dwUserIndex;
    return DeviceIo(pDevice->hDevice, IOCTL_XINPUT_POWER_DOWN_DEVICE, &buff, sizeof(buff), NULL, 0, NULL);
}

HRESULT PowerOffDeviceController(XInputInternalDevice* pDevice, void* param, ULONGLONG arg)
{
    if (!(pDevice->status & STATUS_CONNECTED))
        return E_FAIL;

    return PowerOffController(pDevice);
}

HRESULT ResizeAllocatedDevices(DWORD new_size)
{
    XInputInternalDevice** newStates; // [rsp+20h] [rbp-28h]

    newStates = (XInputInternalDevice**)XInputAlloc(sizeof(*newStates) * new_size);
    if (!newStates)
        return E_OUTOFMEMORY;
    memcpy(newStates, s_AllocatedDevices, sizeof(*s_AllocatedDevices) * s_AllocatedDevicesCount);
    XInputFree(s_AllocatedDevices);
    s_AllocatedDevices = newStates;
    s_AllocatedDevicesCount = new_size;
    return 0;
}

// GetDeviceIndex

HRESULT GetDeviceLed(XInputInternalDevice* pDevice, BYTE* param)
{
    buff_in_t outBuff = { 0 };
    buff_in_t inBuff;
    HRESULT res;

    *param = 0;
    if (pDevice->type < 257)
        return NOERROR;
    inBuff.field_0 = 257;
    inBuff.field_2 = pDevice->dwUserIndex;

    res = DeviceIo(pDevice->hDevice, IOCTL_XINPUT_GET_LED_STATE, &inBuff, sizeof(inBuff), &outBuff, sizeof(outBuff), NULL);
    if (res >= 0)
        *param = outBuff.field_2;
    return res;
}

DWORD IsDeviceFree(DWORD index)
{
    return (index >= s_AllocatedDevicesCount || s_AllocatedDevices[index] == NULL);
}

HRESULT SendLEDState(XInputInternalDevice* pDevice, BYTE num)
{
    BYTE inBuffer[5] = { 0 };

    inBuffer[0] = pDevice->dwUserIndex;
    inBuffer[1] = num;
    inBuffer[4] = 1;
    return DeviceIo(pDevice->hDevice, IOCTL_XINPUT_SET_GAMEPAD_STATE, inBuffer, sizeof(inBuffer), NULL, 0, NULL);
}

DWORD DevicePathExists(XInputInternalDevice* pInternalDevice)
{
    unsigned int i;
    for (i = 0; i < s_AllocatedDevicesCount; ++i)
    {
        if (s_AllocatedDevices[i] && pInternalDevice->dwUserIndex == s_AllocatedDevices[i]->dwUserIndex && s_AllocatedDevices[i]->str && pInternalDevice->str)
        {
            if (wcscmp(s_AllocatedDevices[i]->str, pInternalDevice->str) == 0)
                return 1;
        }
    }
    return 0;
}

HRESULT DeallocateDevices()
{
    if (s_AllocatedDevicesCount == 0)
        return E_FAIL;

    s_AllocatedDevicesCount = 0;

    if (s_AllocatedDevices == NULL)
        return E_FAIL;

    XInputFree(s_AllocatedDevices);
    s_AllocatedDevices = NULL;

    return NOERROR;
}

BOOLEAN DeinitXInputLibrary(BOOLEAN arg)
{
    InternalEnable(FALSE);
    s_Initialized = FALSE;

    //if( arg == FALSE)
    //  

    DeallocateDevices();
    DeleteCriticalSection(&s_XInputCriticalSection);

    return TRUE;
}

HRESULT TryInsertDevice(XInputInternalDevice* pDevice)
{
    HRESULT res;
    BYTE index;
    unsigned int i;

    res = !(pDevice->status & STATUS_CONNECTED);
    if (res == 1)
        return 1;

    res = DevicePathExists(pDevice);
    if (res < 0)
        return res;
    if (res == TRUE)
        return 1;

    index = 0;
    if (GetDeviceLed(pDevice, &index) >= 0 && index < (sizeof(XINPUT_LED_TO_PORT_MAP) / sizeof(*XINPUT_LED_TO_PORT_MAP) - 1))
    {
        if (XINPUT_LED_TO_PORT_MAP[index] != 255)
        {
            res = IsDeviceFree(XINPUT_LED_TO_PORT_MAP[index]);
            if (res < 0)
                return res;
            if (res != TRUE)
            {
                SendLEDState(pDevice, 13);
            }
        }
    }

    index = 255;
    for (i = 0; i < XUSER_MAX_COUNT && index == 255; ++i)
    {
        res = IsDeviceFree(i);
        if (res < 0)
            return (unsigned int)res;
        if (res == 1)
            index = i;
    }
    if (index >= XUSER_MAX_COUNT)
        return 1;
    
    
    if (index < s_AllocatedDevicesCount || (res = ResizeAllocatedDevices(index + 1)) >= 0)
    {
        if (s_AllocatedDevices[index])
            return E_FAIL;

        s_AllocatedDevices[index] = pDevice;
        if (s_LibModifier & 1)
            SendLEDState(pDevice, XINPUT_PORT_TO_LED_MAP[index % MAX_XINPUT_PORT_TO_LED_MAP]);
    }

    return NOERROR;
}

HRESULT CreateInternalDevice(HANDLE hDev, LPWSTR str)
{
    XInputInternalDevice* pDevice;
    buff_init_t init_buff;
    int i;
    HRESULT res;

    memset(&init_buff, 0, sizeof(init_buff));
    if ((res = DeviceIo(hDev, IOCTL_XINPUT_GET_INFORMATION, 0, 0, &init_buff, sizeof(init_buff), 0)) < 0)
        return res;

    if (init_buff.field_4 < 0)
        return res;

    pDevice = NULL;
    for (i = 0; i < init_buff.field_2; ++i)
    {
        if (pDevice)
        {
            HANDLE hDevice = pDevice->hDevice;
            LPWSTR devicePath = pDevice->str;
            memset(pDevice, 0, sizeof(*pDevice));
            pDevice->hDevice = hDevice;
            pDevice->handle2 = INVALID_HANDLE_VALUE;
            pDevice->str = devicePath;
            pDevice->dwUserIndex = 255;
        }
        else
        {
            pDevice = CreateDevice(hDev, str);
            if (!pDevice)
                return E_OUTOFMEMORY;
        }
        pDevice->dwUserIndex = i;
        pDevice->product_id = init_buff.product_id;
        pDevice->vendor_id = init_buff.vendor_id;
        pDevice->type = init_buff.type;
        res = GetDeviceState(pDevice);
        if (res >= 0)
        {
            res = TryInsertDevice(pDevice);
            if (res == NOERROR)
                pDevice = NULL;
        }
    }
    if (pDevice)
        DeleteDevice(pDevice);
    
    return res;
}

HRESULT DiscoverDevices()
{
    HANDLE hObject;
    PSP_DEVICE_INTERFACE_DETAIL_DATA_W pDevIfaceDetailData;
    DWORD memberIndex;
    SP_DEVICE_INTERFACE_DATA pDevIface;
    HDEVINFO hDev;
    HRESULT res;

    hDev = INVALID_HANDLE_VALUE;
    memberIndex = 0;
    pDevIfaceDetailData = NULL;
    res = GetClassDev(&hDev);
    if (res < 0)
        return res;

    while ((res = GetDevInterface(hDev, memberIndex, &pDevIface)) >= 0)
    {
        ++memberIndex;
        res = GetGamepadDetail(hDev, &pDevIface, &pDevIfaceDetailData);
        if (res >= 0)
        {
            hObject = CreateFileW(pDevIfaceDetailData->DevicePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
            if (hObject != INVALID_HANDLE_VALUE)
            {
                res = CreateInternalDevice(hObject, pDevIfaceDetailData->DevicePath);
                if (res < 0)
                    res = 0;
                CloseHandle(hObject);
            }
        }
    }

    res = NOERROR;
    if (hDev != INVALID_HANDLE_VALUE)
    {
        DestroyDiDeviceInfoList(hDev);
        hDev = INVALID_HANDLE_VALUE;
    }
    return res;
}

HRESULT GetOrDiscoverDevice(DWORD dwUserIndex, XInputInternalDevice** ppDevice, BYTE retry_count)
{
    HRESULT res = 0;
    XInputInternalDevice* pDevice = NULL;

    if (dwUserIndex < s_AllocatedDevicesCount)
        pDevice = s_AllocatedDevices[dwUserIndex];

    *ppDevice = pDevice;
    while (pDevice == NULL && retry_count != 0 && (res = DiscoverDevices()) >= 0)
    {
        if (dwUserIndex < s_AllocatedDevicesCount)
            pDevice = s_AllocatedDevices[dwUserIndex];
        else
            pDevice = NULL;

        *ppDevice = pDevice;
        --retry_count;
    }

    return res;
}

HRESULT CleanDeadDevice(DWORD dwUserIndex)
{
    if (dwUserIndex >= s_AllocatedDevicesCount)
        return NOERROR;

    if (s_AllocatedDevices[dwUserIndex])
    {
        DeleteDevice(s_AllocatedDevices[dwUserIndex]);
        s_AllocatedDevices[dwUserIndex] = NULL;
    }
    return NOERROR;
}

HRESULT DoXInputAction(DWORD dwUserIndex, action_func_t func, void* arg, DWORD val)
{
    HRESULT res;

    XInputInternalDevice* pDevice = NULL;
    BOOL retry = TRUE;

    if (s_Initialized == FALSE)
        return E_FAIL;

    EnterCriticalSection(&s_XInputCriticalSection);

    res = GetOrDiscoverDevice(dwUserIndex, &pDevice, val > 0);
    if (res >= 0)
    {
        if (pDevice != NULL)
        {
            res = func(pDevice, arg, val);
            if (res < 0)
            {
                pDevice->status &= ~STATUS_CONNECTED;
            }
            if (res == 1)
            {
                res = NOERROR;
            }
            else
            {
                if (val > 0)
                {
                    if (!(pDevice->status & STATUS_CONNECTED))
                    {
                        res = CleanDeadDevice(dwUserIndex);
                    }
                }
            }
        }
        else
        {
            res = 1;
        }
    }

    LeaveCriticalSection(&s_XInputCriticalSection);

    return res;
}

HRESULT CheckReturnCode(INT code)
{
    if (code < 0)
    {
        switch (code)
        {
            case E_NOTIMPL: return ERROR_CALL_NOT_IMPLEMENTED;
            case E_OUTOFMEMORY: return ERROR_OUTOFMEMORY;
            case E_INVALIDARG: return ERROR_BAD_ARGUMENTS;
                return code;
        }
    }

    if (code == 1)
        return ERROR_DEVICE_NOT_CONNECTED;

    return ERROR_SUCCESS;
}

//////////////////////////////////////////////
// Exported functions

DWORD WINAPI XInputGetStateEx(_In_ DWORD dwUserIndex, _Out_ XINPUT_STATE* pState)
{
    if (dwUserIndex >= XUSER_MAX_COUNT || pState == NULL)
        return ERROR_BAD_ARGUMENTS;
        
    return CheckReturnCode(DoXInputAction(dwUserIndex, pXInputGetStateFunc, (void**)&pState, 1u));
}

DWORD WINAPI XInputGetState(_In_ DWORD dwUserIndex, _Out_ XINPUT_STATE* pState)
{
    DWORD res = XInputGetStateEx(dwUserIndex, pState);

    if(res == ERROR_SUCCESS)
        pState->Gamepad.wButtons &= XINPUT_BUTTON_MASK_WITHOUT_GUIDE;

    return res;
}

DWORD WINAPI XInputSetState(_In_ DWORD dwUserIndex, _Out_ XINPUT_VIBRATION *pVibration)
{
    if (dwUserIndex >= XUSER_MAX_COUNT || pVibration == NULL)
        return ERROR_BAD_ARGUMENTS;

    return CheckReturnCode(DoXInputAction(dwUserIndex, pXInputSetStateFunc, (void**)&pVibration, TRUE));
}

DWORD WINAPI XInputGetCapabilities(_In_ DWORD dwUserIndex, _In_ DWORD dwFlags, _Out_ XINPUT_CAPABILITIES *pCapabilities)
{
    if (dwUserIndex >= XUSER_MAX_COUNT || (dwFlags != 0 && dwFlags != XINPUT_FLAG_GAMEPAD) || pCapabilities == NULL)
        return ERROR_BAD_ARGUMENTS;

    return CheckReturnCode(DoXInputAction(dwUserIndex, XInputGetCapabilitiesEnabled, (void**)&pCapabilities, TRUE));
}

DWORD WINAPI XInputGetKeystroke(_In_ DWORD dwUserIndex, _In_ DWORD dwReserved, _Out_ XINPUT_KEYSTROKE *pKeystroke)
{
    internal_keystroke_arg_t arg;
    DWORD res;
    DWORD res_arg;

    res = ERROR_SUCCESS;

    if (dwUserIndex >= XUSER_MAX_COUNT && dwUserIndex != XUSER_INDEX_ANY || pKeystroke == NULL)
        return ERROR_BAD_ARGUMENTS;

    arg.reserved = &dwReserved;
    arg.xinput_keystroke = pKeystroke;
    arg.result = &res_arg;
    res_arg = ERROR_EMPTY;
    if (dwUserIndex == XUSER_INDEX_ANY)
    {
        int i;
        for (i = 0; i < XUSER_MAX_COUNT; ++i)
        {
            DoXInputAction(i, XInputGetKeystrokeEnabled, &arg, 3);
            if (res_arg)
            {
                pKeystroke->UserIndex = i;
                break;
            }
        }
        if (i == XUSER_MAX_COUNT)
            return ERROR_EMPTY;
    }
    else
    {
        res = DoXInputAction(dwUserIndex, XInputGetKeystrokeEnabled, &arg, 3);
        if (res >= 0)
        {
            if (res != 1)
                pKeystroke->UserIndex = (BYTE)dwUserIndex;
            else
                res_arg = ERROR_DEVICE_NOT_CONNECTED;
        }
    }

    return res_arg;
}

DWORD WINAPI XInputGetBatteryInformation(_In_ DWORD dwUserIndex, _In_ BYTE devType, _Out_ XINPUT_BATTERY_INFORMATION * pBatteryInformation)
{
    XInputInternalDevice internalDevice;
    battery_info batt_info;
    internal_battery_info_arg_t arg;
    BOOL retry;
    BOOLEAN doDeviceAction = TRUE;
    int res = ERROR_SUCCESS;
    HANDLE hFile;

    if (dwUserIndex >= XUSER_MAX_COUNT || pBatteryInformation == NULL)
        return ERROR_BAD_ARGUMENTS;

    if (devType == BATTERY_DEVTYPE_HEADSET)
    {
        ResetBatteryInfo(&batt_info);
        retry = InitBatteryInfo(&batt_info);

        res = ERROR_DEVICE_NOT_CONNECTED;
        while(retry)
        {
            retry = OpenBatteryDevice(&batt_info, &hFile);
            if (hFile != INVALID_HANDLE_VALUE)
            {
                if (InitInternalDeviceFromHANDLE(hFile, &internalDevice) && internalDevice.type >= 258)
                {
                    internalDevice.dwUserIndex = (BYTE)dwUserIndex;
                    if (GetBatteryInfo(&internalDevice, BATTERY_DEVTYPE_HEADSET, pBatteryInformation) >= 0)
                    {
                        retry = FALSE;
                        doDeviceAction = FALSE;
                    }
                }
                CloseHandle(hFile);
            }
        }
        DeinitBatteryInfo(&batt_info);
    }
    if (doDeviceAction)
    {
        arg.devType = &devType;
        arg.pBatteryInformation = pBatteryInformation;
        return CheckReturnCode(DoXInputAction(dwUserIndex, GetDeviceBatteryInfo, (void*)&arg, 2));
    }

    return res;
}

DWORD WINAPI XInputGetDSoundAudioDeviceGuids(_In_ DWORD dwUserIndex, _Out_ GUID* pDSoundRenderGuid, _Out_ GUID* pDSoundCaptureGuid)
{
    if (dwUserIndex >= XUSER_MAX_COUNT || pDSoundCaptureGuid == NULL || pDSoundRenderGuid == NULL)
        return ERROR_BAD_ARGUMENTS;

    GUID* pGuids[2];

    pGuids[0] = pDSoundRenderGuid;
    pGuids[1] = pDSoundCaptureGuid;

    battery_info batt_info;
    ResetBatteryInfo(&batt_info);
    BOOLEAN continue_ = InitBatteryInfo(&batt_info);
    BOOLEAN doDeviceAction = TRUE;
    DWORD res = ERROR_DEVICE_NOT_CONNECTED;
    while (continue_)
    {
        HANDLE hDevice;
        continue_ = OpenBatteryDevice(&batt_info, &hDevice);
        if (hDevice != INVALID_HANDLE_VALUE)
        {
            XInputInternalDevice internal_device;
            if (InitInternalDeviceFromHANDLE(hDevice, &internal_device) && internal_device.type >= 258)
            {
                internal_device.dwUserIndex = (BYTE)dwUserIndex;
                if (GetDeviceAudioIds(&internal_device, pGuids, 2) >= 0)
                {
                    continue_ = FALSE;
                    doDeviceAction = FALSE;
                    res = ERROR_SUCCESS;
                }
            }
        }
    }
    DeinitBatteryInfo(&batt_info);

    if (doDeviceAction)
    {
        return CheckReturnCode(DoXInputAction(dwUserIndex, GetDeviceAudioIds, (void*)pGuids, 2));
    }

    return res;
}

void WINAPI XInputEnable(DWORD enable)
{
    InternalEnable(enable != 0);
}

DWORD WINAPI XInputWaitForGuideButton(DWORD a, DWORD b, DWORD c)
{
    return ERROR_ACCESS_DENIED;
}

DWORD WINAPI XInputCancelGuideButtonWait(DWORD a)
{
    return ERROR_ACCESS_DENIED;
}

DWORD WINAPI XInputPowerOffController(_In_ DWORD dwUserIndex)
{
    if (dwUserIndex >= XUSER_MAX_COUNT)
        return ERROR_BAD_ARGUMENTS;

    return CheckReturnCode(DoXInputAction(dwUserIndex, PowerOffDeviceController, NULL, 0));
}

DWORD WINAPI XInputGetMaxController()
{
    return XUSER_MAX_COUNT;
}

BOOL WINAPI DllMain(
    _In_ HINSTANCE hinstDLL,
    _In_ DWORD     fdwReason,
    _In_ LPVOID    lpvReserved
)
{
    BOOL res = TRUE;
    switch (fdwReason)
    {
        case DLL_PROCESS_DETACH: return DeinitXInputLibrary(lpvReserved != NULL);
        case DLL_PROCESS_ATTACH: return InitXInputLibrary();
        case DLL_THREAD_ATTACH: break;
        case DLL_THREAD_DETACH: break;
        default:
            res = HookLibraryFunc(fdwReason, lpvReserved);
            if (res == FALSE)
            {
                res = ModifyLibrary(fdwReason, lpvReserved);
            }
    }
    return res;
}