/* Copyright (C) 2019-2020 Nemirtingas
 * This file is part of the xinput open-source project.
 *
 * xinput open-source project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * xinput open-source project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with xinput open-source project.  If not, see <https://www.gnu.org/licenses/>
 */

#pragma once

// Build guids, don't need to link against other dll
#define INITGUID

#include <Windows.h>
#include <SetupAPI.h>
#include <Xinput.h>
#include <dsound.h>
#include <dsconf.h>
#include <stdio.h>

#pragma comment(lib, "Setupapi.lib")

#pragma warning(disable : 4995)
#pragma warning(disable : 4996)

#ifdef XUSER_MAX_COUNT
#undef XUSER_MAX_COUNT
#endif

#define XUSER_MAX_COUNT 8

#define STATUS_CONNECTED 1
#define STATUS_2 2

typedef struct _XInputInternalDevice
{
    int status;
    int field_4;
    HANDLE hDevice;
    HANDLE handle2;
    BYTE dwUserIndex;
    char field_19;
    char field_1A;
    char field_1B;
    char field_1C;
    char field_1D;
    char field_1E;
    char field_1F;
    LPWSTR str;
    SIZE_T strlen;
    int field_30;
    int field_34;
    WORD type;
    char field_3A;
    char field_3B;
    XINPUT_STATE xinput_state;
    XINPUT_VIBRATION xinput_vibration;
    int field_50;
    int field_54;
    int field_58;
    int field_5C;
    int field_60;
    char field_64;
    char field_65;
    WORD vendor_id;
    WORD product_id;
    BYTE input_id;
    BYTE trigger_key;
    WORD button_keys;
    WORD left_stick_key;
    WORD right_stick_key;
    WORD field_72;
    int field_74;
} XInputInternalDevice;

#pragma pack(push, 1)

typedef struct _battery_info
{
    HANDLE hDev;
    DWORD memberIndex;
    PSP_DEVICE_INTERFACE_DETAIL_DATA_W deviceInterfaceDetail;
    SIZE_T deviceInterfaceDetailSize;
} battery_info;

typedef struct _buff_out_xbox_one_t
{
    BYTE  status;
    BYTE  field_1;
    BYTE  input_id;
    DWORD dwPacketNumber;
    BYTE  field_7;
    WORD  wButtons;
    BYTE  bLeftTrigger;
    BYTE  bRightTrigger;
    WORD  sThumbLX;
    WORD  sThumbLY;
    WORD  sThumbRX;
    WORD  sThumbRY;
} buff_out_unknown_dev_t;

typedef struct _buff_out_xbox_t
{
    BYTE  field_0;
    BYTE  field_1;
    BYTE  status;
    BYTE  field_3;
    BYTE  input_id;
    DWORD dwPacketNumber;
    BYTE  field_9;
    BYTE  field_A;
    WORD  wButtons;
    BYTE  bLeftTrigger;
    BYTE  bRightTrigger;
    WORD  sThumbLX;
    WORD  sThumbLY;
    WORD  sThumbRX;
    WORD  sThumbRY;
    BYTE  field_17;
    BYTE  field_18;
    BYTE  field_19;
    BYTE  field_1A;
    BYTE  field_1B;
    BYTE  field_1C;
} buff_out_xbox_t;

typedef struct _buff_in_t
{
    SHORT field_0;
    BYTE  field_2;
} buff_in_t;

typedef struct _buff_vibration_t
{
    BYTE field_0;
    BYTE field_1;
    BYTE left_speed;
    BYTE right_speed;
    BYTE field_4;
} buff_vibration_t;

typedef struct _buff_init_t
{
    WORD type;
    BYTE field_2;
    BYTE field_3;
    BYTE field_4;
    BYTE field_5;
    WORD field_6;
    WORD vendor_id;
    WORD product_id;
} buff_init_t;

typedef struct _device_ids_t
{
    WORD vendor_id;
    WORD product_id;
    BYTE input_id;
} device_ids_t;

typedef struct _buff_ids_t
{
    WORD field_0;
    device_ids_t ids;
} buff_ids_t;

typedef struct _buff_in_battery_t
{
    WORD type;
    BYTE field_2;
    BYTE devType;
} buff_in_battery_t;

typedef struct _buff_out_battery_t
{
    WORD type;
    BYTE BatteryType;
    BYTE BatteryLevel;
} buff_out_battery_t;

typedef struct _buff_capabilities_t
{
    BYTE field_0;
    BYTE field_1;
    BYTE Type;
    BYTE SubType;
    WORD wButtons;
    BYTE bLeftTrigger;
    BYTE bRightTrigger;
    SHORT sThumbLX;
    SHORT sThumbLY;
    SHORT sThumbRX;
    SHORT sThumbRY;
    DWORD field_10;
    BYTE field_14;
    BYTE field_15;
    BYTE wLeftMotorSpeed;
    BYTE wRightMotorSpeed;
} buff_capabilities_t;

typedef struct _internal_keystroke_arg_t
{
    DWORD* reserved;
    XINPUT_KEYSTROKE* xinput_keystroke;
    DWORD* result;
} internal_keystroke_arg_t;

typedef struct _internal_battery_info_arg_t
{
    BYTE* devType;
    XINPUT_BATTERY_INFORMATION* pBatteryInformation;
} internal_battery_info_arg_t;

#pragma pack(pop)

typedef HRESULT(*action_func_t)(XInputInternalDevice*, void*, ULONGLONG);

typedef struct _dsound_func_param_t
{
    PCWSTR device_path;
    GUID* headphone_guid;
    GUID* microphone_guid;
} dsound_func_param_t;

#define NUM_STATES ((XUSER_MAX_COUNT)*2)

#ifndef XINPUT_GAMEPAD_GUIDE
#define XINPUT_GAMEPAD_GUIDE 0x0400
#endif

#ifndef XINPUT_GAMEPAD_GUIDE
#define XINPUT_GAMEPAD_GUIDE 0x0400
#endif

#define XINPUT_BUTTON_MASK_WITHOUT_GUIDE \
    ((XINPUT_GAMEPAD_DPAD_UP)|(XINPUT_GAMEPAD_DPAD_DOWN)|(XINPUT_GAMEPAD_DPAD_LEFT)|(XINPUT_GAMEPAD_DPAD_RIGHT)| \
    (XINPUT_GAMEPAD_START)|(XINPUT_GAMEPAD_BACK)|(XINPUT_GAMEPAD_LEFT_THUMB)|(XINPUT_GAMEPAD_RIGHT_THUMB)| \
    (XINPUT_GAMEPAD_LEFT_SHOULDER)|(XINPUT_GAMEPAD_RIGHT_SHOULDER)| \
    (XINPUT_GAMEPAD_A)|(XINPUT_GAMEPAD_B)|(XINPUT_GAMEPAD_X)|(XINPUT_GAMEPAD_Y))

#define XINPUT_BUTTON_MASK ((XINPUT_BUTTON_MASK_WITHOUT_GUIDE)|XINPUT_GAMEPAD_GUIDE)

#define MAX_DEVICES 16

#define IOCTL_XINPUT_BASE  0x8000

#define XINPUT_READ_ACCESS  ( FILE_READ_ACCESS )
#define XINPUT_WRITE_ACCESS ( FILE_WRITE_ACCESS )
#define XINPUT_RW_ACCESS    ( (XINPUT_READ_ACCESS) | (XINPUT_WRITE_ACCESS) )

#define IOCTL_XINPUT_GET_INFORMATION         CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x00, XINPUT_READ_ACCESS)  // 0x80006000
#define IOCTL_XINPUT_GET_CAPABILITIES        CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x04, XINPUT_RW_ACCESS)    // 0x8000E004
#define IOCTL_XINPUT_GET_LED_STATE           CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x08, XINPUT_RW_ACCESS)    // 0x8000E008
#define IOCTL_XINPUT_GET_GAMEPAD_STATE       CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x0C, XINPUT_RW_ACCESS)    // 0x8000E00C
#define IOCTL_XINPUT_SET_GAMEPAD_STATE       CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x10, XINPUT_WRITE_ACCESS) // 0x8000A010
#define IOCTL_XINPUT_WAIT_FOR_GUIDE_BUTTON   CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x14, XINPUT_WRITE_ACCESS) // 0x8000A014
#define IOCTL_XINPUT_GET_BATTERY_INFORMATION CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x18, XINPUT_RW_ACCESS)    // 0x8000E018
#define IOCTL_XINPUT_POWER_DOWN_DEVICE       CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x1C, XINPUT_WRITE_ACCESS) // 0x8000A010
#define IOCTL_XINPUT_GET_AUDIO_INFORMATION   CTL_CODE(IOCTL_XINPUT_BASE, 0x800, 0x20, XINPUT_RW_ACCESS)    // 0x8000E020

#define XINPUT_LED_OFF            ((BYTE)0)
#define XINPUT_LED_BLINK          ((BYTE)1)
#define XINPUT_LED_1_SWITCH_BLINK ((BYTE)2)
#define XINPUT_LED_2_SWITCH_BLINK ((BYTE)3)
#define XINPUT_LED_3_SWITCH_BLINK ((BYTE)4)
#define XINPUT_LED_4_SWITCH_BLINK ((BYTE)5)
#define XINPUT_LED_1              ((BYTE)6)
#define XINPUT_LED_2              ((BYTE)7)
#define XINPUT_LED_3              ((BYTE)8)
#define XINPUT_LED_4              ((BYTE)9)
#define XINPUT_LED_CYCLE          ((BYTE)10)
#define XINPUT_LED_FAST_BLINK     ((BYTE)11)
#define XINPUT_LED_SLOW_BLINK     ((BYTE)12)
#define XINPUT_LED_FLIPFLOP       ((BYTE)13)
#define XINPUT_LED_ALLBLINK       ((BYTE)14)
static BYTE XINPUT_PORT_TO_LED_MAP[] =
{
    XINPUT_LED_1,
    XINPUT_LED_2,
    XINPUT_LED_3,
    XINPUT_LED_4,
};
#define MAX_XINPUT_PORT_TO_LED_MAP (sizeof(XINPUT_PORT_TO_LED_MAP)/sizeof(*XINPUT_PORT_TO_LED_MAP))

static BYTE XINPUT_LED_TO_PORT_MAP[] =
{
    0xFF, 0xFF, 0, 1, 2, 3, 0, 1, 2, 3, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0
};

#define DEFINE_HIDDEN_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        static const GUID name = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }

DEFINE_HIDDEN_GUID(NULL_GUID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DEFINE_HIDDEN_GUID(XUSB_INTERFACE_CLASS_GUID, 0xEC87F1E3, 0xC13B, 0x4100, 0xB5, 0xF7, 0x8B, 0x84, 0xD5, 0x42, 0x60, 0xCB);

static const GUID null_guid = { 0 };

typedef DWORD(WINAPI* XInputGetState_t)(_In_ DWORD, _Out_ XINPUT_STATE*);
typedef DWORD(WINAPI* XInputSetState_t)(_In_ DWORD, _In_ XINPUT_VIBRATION*);
typedef DWORD(WINAPI* XInputGetCapabilities_t)(_In_ DWORD, _In_ DWORD, _Out_ XINPUT_CAPABILITIES*);
typedef void (WINAPI* XInputEnable_t)(_In_ DWORD);
typedef DWORD(WINAPI* XInputGetAudioDeviceIds_t)(_In_ DWORD, _Out_writes_opt_(*pRenderCount) LPWSTR, _Inout_opt_ UINT* pRenderCount, _Out_writes_opt_(*pCaptureCount) LPWSTR, _Inout_opt_ UINT* pCaptureCount);
typedef DWORD(WINAPI* XInputGetBatteryInformation_t)(_In_ DWORD, _In_ BYTE, _Out_ XINPUT_BATTERY_INFORMATION*);
typedef DWORD(WINAPI* XInputGetKeystroke_t)(_In_ DWORD, _Reserved_ DWORD, _Out_ PXINPUT_KEYSTROKE);

#ifdef __cplusplus
extern "C" {
#endif

DWORD WINAPI XInputGetStateEx(_In_ DWORD dwUserIndex, _Out_ XINPUT_STATE* pState);

DWORD WINAPI XInputWaitForGuideButton(DWORD a, DWORD b, DWORD c);

DWORD WINAPI XInputCancelGuideButtonWait(DWORD a);

DWORD WINAPI XInputPowerOffController(_In_ DWORD dwUserIndex);

DWORD WINAPI XInputGetMaxController();

#ifdef __cplusplus
}
#endif